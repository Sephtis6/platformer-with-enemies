﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpTrigger : MonoBehaviour {

	public AudioClip soundToPlay;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
        // Pickup
        AudioSource.PlayClipAtPoint(soundToPlay, GetComponent<Transform>().position, 1.0f);
        // Add to score
        Destroy(gameObject);

        // Set my checkpoint to this pickup
        GameManager.instance.respawnPoint = GetComponent<Transform>().position;
        GameManager.instance.score = GameManager.instance.score + 1;
    }

}
