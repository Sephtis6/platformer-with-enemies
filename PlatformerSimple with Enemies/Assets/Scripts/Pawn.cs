﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : MonoBehaviour {

    //make a movement vector for all pawns
	public virtual void Move (Vector2 moveVector) {
	}
    //make a jump vector for all pawns
    public virtual void Jump () {
	}
		
}
