﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetSpawnPoint : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameManager.instance.respawnPoint = GetComponent<Transform>().position;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
