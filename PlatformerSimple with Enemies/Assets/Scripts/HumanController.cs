﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanController : Controller {

	public KeyCode jumpKey = KeyCode.W;
	public KeyCode D = KeyCode.D;
	public KeyCode A = KeyCode.A;
	public KeyCode W = KeyCode.W;
	public KeyCode S = KeyCode.S;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		// Aggregate movement each frame, and always move
		Vector2 movementVector = Vector2.zero;
		if (Input.GetKey (A)) {
			movementVector.x = -1;
		}
		if (Input.GetKey (D)) {
			movementVector.x = 1;
		}
		if (Input.GetKey(W)) {
			movementVector.y = 1;
		}
		if (Input.GetKey(S)) {
			movementVector.y = -1;
		}

		pawn.Move (movementVector);

		// Jump
		if (Input.GetKeyDown (jumpKey)) {
			pawn.Jump ();
		}

	}
}
