﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotPawn : Pawn {

    //integers and floats named
	private Rigidbody2D rb;
	private SpriteRenderer sr;
	private Transform tf;
	private Animator anim;

	public float moveSpeed;
	public float jumpForce;
	public bool isGrounded;
	public float groundDistance = 0.1f;
    public float jumpsLeft;
    private float jumpsAvailable;

	public void Start()
    {
		// Get my components
		rb = GetComponent<Rigidbody2D>();
		sr = GetComponent<SpriteRenderer> ();
		tf = GetComponent<Transform> ();
		anim = GetComponent<Animator> ();
        jumpsAvailable = jumpsLeft;
	}

	public void Update()
    {
		// Check for Grounded
		CheckForGrounded();
	}

    //checking for grounded using raycast
	public void CheckForGrounded () {
		RaycastHit2D hitInfo = Physics2D.Raycast (tf.position, Vector3.down, groundDistance);
		if (hitInfo.collider == null)
        {
			isGrounded = false;
		}
        else
        {
			isGrounded = true;
			Debug.Log ("Standing on " + hitInfo.collider.name);
		}
	}

	public override void Move (Vector2 moveVector) {
		// Change X velocity based on speed and moveVector
		rb.velocity = new Vector2 (moveVector.x * moveSpeed, rb.velocity.y);

		// If velocity is <0, flip sprite. 
		if (rb.velocity.x < 0) {
			anim.Play ("RobotWalk");
			sr.flipX = true;
		}
        // If velocity is >0, flip sprite. 
        else if (rb.velocity.x > 0) {
			anim.Play ("RobotWalk");
			sr.flipX = false;
		}
        // If velocity is not grounded play jump anim 
        else if (isGrounded == false)
        {
            anim.Play("RobotJump");
        }
        // If velocity is 0 set to idle
        else
        {
			anim.Play ("RobotIdle");
		}
	}

    //allows the character pawn to jump/double jump
	public override void Jump () {
		if (isGrounded) {
            rb.AddForce (Vector2.up * jumpForce, ForceMode2D.Force);
            jumpsLeft = jumpsAvailable;
        }
        if (isGrounded == false)
        {
            if (jumpsLeft > 0)
            {
                rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Force);
                jumpsLeft = jumpsLeft - 1;
            }
            if (jumpsLeft == 0)
            {
                jumpsLeft = jumpsLeft - 1;
            }
        }
	}

}
