﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PawnLives : MonoBehaviour
{

    private SpriteRenderer sr;
    int targetvalue;
    int hitPoints;
    int life;
    int points;
    public Transform tf;
    public AudioClip soundDeath;
    public AudioClip soundHit;

    // Use this for initialization
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        targetvalue = 0;
        hitPoints = 3;
        life = 3;
        tf = GetComponent<Transform>();
        points = hitPoints;

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        //if the player is out of lives it will destroy the player's ship and exit the game application
        if (life > targetvalue)
        {
            AudioSource.PlayClipAtPoint(soundHit, GetComponent<Transform>().position, 1.0f);
            AudioSource.PlayClipAtPoint(soundDeath, GetComponent<Transform>().position, 1.0f);
            tf.position = GameManager.instance.respawnPoint;
            --life;
            hitPoints = points;
        }
        //if hit the ship/player loses all life
        if (life == 0)
        {
        SceneManager.LoadScene(4);
        }

        // if (life > targetvalue)
        // {
        //    AudioSource.PlayClipAtPoint(soundToPlay, GetComponent<Transform>().position, 1.0f);
        //     tf.position = GameManager.instance.respawnPoint;
        //    --life;
        // }
        //if the player is out of lives it will destroy the player's ship and exit the game application
        // if (life == targetvalue)
        //{
        // Destroy(gameObject);
        //UnityEditor.EditorApplication.isPlaying = false;
        //Application.Quit();
        //}
        //if hit the ship/player loses a life
        // if (hitPoints > targetvalue)
        //{
        //    AudioSource.PlayClipAtPoint(soundHit, GetComponent<Transform>().position, 1.0f);
        //    --hitPoints;
        //}
        //if the player is out of lives it will destroy the player's ship and exit the game application
        //if (hitPoints == targetvalue)
        //{
        //   AudioSource.PlayClipAtPoint(soundDeath, GetComponent<Transform>().position, 1.0f);
        //   tf.position = GameManager.instance.respawnPoint;
        //   --life;
        //   hitPoints = points;
        //}
    }
    void OnCollisionEnter2D()
    {
        Debug.Log("You have been DESTROYED");
    }
}
