﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class lvl3toEndScreen : MonoBehaviour {

	public AudioClip soundToPlay;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        // Pickup
        AudioSource.PlayClipAtPoint(soundToPlay, GetComponent<Transform>().position, 1.0f);
        GameManager.instance.score = GameManager.instance.score + 1;
        // Add to score
        Destroy(gameObject);

        // Send to next level
        SceneManager.LoadScene(5);
    }

}
