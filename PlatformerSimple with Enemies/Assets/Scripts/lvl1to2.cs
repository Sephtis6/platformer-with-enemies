﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class lvl1to2 : MonoBehaviour {

	public AudioClip soundToPlay;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        // Pickup
        AudioSource.PlayClipAtPoint(soundToPlay, GetComponent<Transform>().position, 1.0f);
        GameManager.instance.score = GameManager.instance.score + 1;
        GameManager.instance.respawnPoint = GetComponent<Transform>().position;
        // Add to score
        Destroy(gameObject);

        // Send player to next level
        SceneManager.LoadScene(2);
    }

}
